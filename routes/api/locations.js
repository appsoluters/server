const express = require('express');
const router = express.Router();
const Joi = require('joi');

//Locations model
const Location = require('../../models/Location');

// @router GET api/locations
// @desc Get all locations
// @access Public
router.get('/', (req, res, next) => {
   Location.find()
           .then(result => res.json(result))
           .catch(err => {
             next(err)
           })
});

//validation schema for media
const locSchema = Joi.object().keys({
    locationId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
    path: Joi.string().required(),
    type: Joi.string().required()
});

// @router POST api/locations/uploadMedia
// @desc add media to location
// @access Public
router.post('/uploadMedia', (req, res, next) => {
  const validate = Joi.validate(req.body, locSchema);

  if (validate.error) {
    next(validate.error.details[0]);
  } else {
    Location.findOneAndUpdate(
        { "_id": req.body.locationId },
        { $push: {
                  media: {
                     $each: [{ path: req.body.path, type: req.body.type }],
                     $sort: { createdAt: -1 }
                   }
                }
        },
        {new: true},
        function(err, doc) {
          if (err) {
            next({message: err.message, status:400});
          } else if(!doc) {
            next({message: "Location not found", status:400});
          } else {
            res.json(doc);
          }
        }
    );
  }

});

module.exports = router
