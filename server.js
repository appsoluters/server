const express = require('express');
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const bodyParser = require('body-parser');

//Routes
const locations = require('./routes/api/locations');

const app = express();

//bodyperser middleware
app.use(bodyParser.json());
const db = require('./config/constants').mongoURI;

//connect to
mongoose.connect(db, { useNewUrlParser: true })
        .then(() => console.log("Mongo db connected ... "))
        .catch(err => console.log(err))

//Use Routes
app.use('/api/locations', locations);

//Catch 404
app.use((req, res, next) => {
  const error = new Error("Endpoint not found");
  error.status = 404;
  next(error);
})

//Error handler
app.use((err, req, res, next) => {
  const status = err.status || 500;

  res.status(status).json({
    error: {
      message: err.message
    }
  });
  
})

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
