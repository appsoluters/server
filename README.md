# StreetLife Test Mobile Application

This is a test server side application for StreetLife.

### Prerequisites

Follwing are needed before running the app

* [NPM](https://www.npmjs.com/)
* [NODEJS](https://nodejs.org/en/)


### Installing

Following are steps for installation

Goto the root directory of the project,

```
npm install
```
