const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MediaSchema = new Schema({
  path: String,
  type: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
})

const LocationSchema = new Schema({
  name: String,
  description: String,
  address: String,
  category: String,
  latitude: Number,
  longitude: Number,
  media: [MediaSchema]
})

module.exports = Location = mongoose.model('locations', LocationSchema);
